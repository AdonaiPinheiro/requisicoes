import React, { Component } from 'react';
import { View, FlatList, StyleSheet, Text } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5'

export default class Requisicoes extends Component {

  constructor(props){
    super(props);
    this.state = {
      nome_cidade:'',
      nome_estado:'',
      data:{}
    }

    //Fazendo uma requisição
    fetch('https://apiadvisor.climatempo.com.br/api/v1/weather/locale/7381/current?token=05851d6b5a46cbe8bd6ea3f72feb6c3e')
      .then((r) => r.json())
      .then((json) => {
        let state = this.state;
        state.nome_cidade = json.name;
        state.nome_estado = json.state;
        state.data = json.data;
        this.setState(state);
      })

  }

  render(){
    return(
      <View style={styles.container}>
        <Icon name="cloud" size={64} />
        <Text style={{ fontSize:24 }}>{this.state.nome_cidade} - {this.state.nome_estado} | Temp.: {this.state.data.temperature}º</Text>
        <Text style={{ fontSize:24 }}>{this.state.data.condition}</Text>
        <Text style={{ fontSize:24 }}>{this.state.data.date}</Text>
      </View>
    );
  }
}

class Filme extends Component {
  render(){
    return(
      <View>
        <Text>{this.props.data.title}</Text>
        <Text>{this.props.data.releaseYear}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container:{
    flex:1,
    justifyContent:'center',
    alignItems:'center'
  }
});